//#define F_CPU     1000000L      //LED flashing frequency in 1Hz

#include <inttypes.h>		//short forms for various integer types
#include <avr/io.h>
#include <avr/interrupt.h>  	//file to be included if using interrupts
#include <util/delay.h>  	//file to be included if using interrupts

// Implicitni taktovani procesoru 1MHz
#define XTAL		1000000L  


// Definice casovych konstant programu
#define LTIME_COUNT	 125 //125
#define HTIME_COUNT  1007 //125

// Promenne pro praci se signalem od svetla
volatile int htime_counter;		// aktualni stav svetelneho senzoru
					// 1 ... svetlo, 0 ... tma
volatile int rele_state;		// posledni hodnota nactena hodnota senzoru

// Inicializace HW systemu pri RESETU
int HWsetup(void)
{


  return(0);


}

// Inicializace stavu mikroprocesoru
int setup(void)
{

  return(0);
}


ISR(TIMER0_COMPA_vect)    // handler for Output Compare 1 overflow interrupt
{
 	htime_counter++;
	if (htime_counter >= 56) {
		PORTB |= (1 << PB0);
		rele_state = 1;
		htime_counter=0;
	}
}
/*
ISR(TIMER1_COMPA_vect) // handler for Output Compare 1 overflow interrupt
{
 		PORTB ^= (1 << PB0);
		rele_state = 1;

}
/*
// Obsluha preruseni pri zmene signalu od svetelneho senzoru - probuzeni systemu
ISR(INT0_vect)
{
}

// Obsluha preruseni pri zmene hodnoty jednoho z vstupnich pinu
ISR(PCINT0_vect)
{


}
*/

/*ISR(TIMER1_OVF_vect) {
	
	TCNT1 = LTIME_COUNT;
	htime_counter++;
	if (htime_counter> HTIME_COUNT) {
//		TCNT1 = LTIME_COUNT;
		htime_counter = 0;
		PORTB |= (1 << PB0);
		rele_state = 1;
	}
}*/

// hlavni program
int main(void)
{
	OSCCAL = 0x42;

	DDRB |= (1 << PB0); // PB0 (pin 5 - output)
	
	// preddelicka 64
/*	TCCR1 |= (1 << CS01) | (1 << CS00);
	TIMSK |= (1 << TOIE1); 

	TCNT1 = LTIME_COUNT;

  // Timer 1
   TCCR1 |= (1 << 7); // Configure timer 1 for CTC mode
   TIMSK |= (1 << OCIE1A); // Enable CTC interrupt
//   OCR1A = 15624; // Set CTC compare value to 1Hz at 1MHz AVR clock, with a prescaler of 64 //CHANGING THIS HAS NO PERCIEVABLE EFFECT
   OCR1C = 125;
   OCR1A = 125;
   TCCR1 |= ( (0 << CS10) | (0 << CS11)| (0 << CS12) | (1 << CS13) ); // Start timer at Fcpu/64 (1 << CS10) | (1 << CS11)| (1 << CS12) | (1 << CS13)
*/	

   TCCR0A = ((0 << COM0A1) | (0 << COM0A0) | (0 << COM0B1) | (0 << COM0B0) | (1 << WGM01) | (1 << WGM00));
   TCCR0B = ((0 << FOC0A) | (0 << FOC0B) | (0 << WGM02) | (0 << CS02) | (1 << CS01) | (1 << CS00));
   OCR0A = 124; //0xff;      //Sets counter to 127 CO
//   OCR0B = 0xff;      //Sets counter to 255
   TIMSK = ((1 << OCIE1A) | (0 << OCIE1B) | (1 << OCIE0A) | (0 << OCIE0B) | (0 << TOIE1) | (0 << TOIE0));

	rele_state=0;
	PORTB &= ~(1 << PB0);
	htime_counter = 0;
	sei();


	while(1) {
		if (rele_state == 1) {
			rele_state = 0;
			_delay_ms(250);
			PORTB &= ~(1 << PB0);
		}
	}
	
  return(0);

}
